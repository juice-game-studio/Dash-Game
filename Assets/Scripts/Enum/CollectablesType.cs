namespace Enum.Collectable
{   
    /// <summary>
    /// Enum des différents types de collectables
    /// </summary>
    public enum CollectablesType
    {
        MONEY, // Collectable de type argent
        SPEED_UP, // Collectable de type Bonus (speed up)
        INVICIBILITY, // Collectable de type Bonus (give invincibility)
        INSTANTE_KILL, // Collectable de type Bonus (instante kill)
        LIFE // Collectable de type Bonus (give life)
    }
}

