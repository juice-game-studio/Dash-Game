namespace Enum.Menu
{   
    /// <summary>
    /// Enum des différents types de menu
    /// </summary>
    public enum MenuType
    {
        MAIN, // Menu principale 
        SHOP, // Menu du shop
        OPTIONS, // Menu des options
        GAME // Menu en jeux
    }
}
