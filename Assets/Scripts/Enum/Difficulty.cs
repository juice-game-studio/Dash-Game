namespace Enum.Difficulty
{   
    /// <summary>
    /// Enum des différents type de difficultés
    /// </summary>
    public enum Difficulty 
    {
        EasierThanEasy,
        Easy,
        Medium,
        Hard,
        HarderThanHard,
        Insane,
        Impossible,
        Hahahahaha
    }
}

