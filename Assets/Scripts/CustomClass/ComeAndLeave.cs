using UnityEngine;

namespace CustomClass
{
    /// <summary>
    /// Class pour remplacer les fonctions Instantiate & Destroy
    /// </summary>
    public class ComeAndLeave
    {
        /// <summary>
        /// Active un GameObject (Remplace Instantiate)
        /// </summary>
        /// <param name="gameObject">Le GameObject a activer</param>
        public static void Come(GameObject gameObject)
        {
            gameObject.transform.position = Vector3.zero;
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Active un GameObject et change son parent (Remplace Instantiate)
        /// </summary>
        /// <param name="gameObject">Le GameObject a activer</param>
        /// <param name="newParent">Le nouveau parent</param>
        public static void Come(GameObject gameObject, Transform newParent)
        {
            gameObject.transform.position = Vector3.zero;
            gameObject.transform.parent = newParent;
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Active un GameObject et change sa position (Remplace Instantiate)
        /// </summary>
        /// <param name="gameObject">Le GameObject a activer</param>
        /// <param name="newPos">La nouvelle position</param>
        public static void Come(GameObject gameObject, Vector3 newPos)
        {
            gameObject.transform.position = newPos;
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Active un GameObject et change sa position et sa rotation (Remplace Instantiate)
        /// </summary>
        /// <param name="gameObject">Le GameObject a activer</param>
        /// <param name="newPos">La nouvelle position</param>
        /// <param name="newRot">La nouvelle rotation</param>
        public static void Come(GameObject gameObject, Vector3 newPos, Quaternion newRot)
        {
            gameObject.transform.position = newPos;
            gameObject.transform.rotation = newRot;
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Active un GameObject et change sa position, sa rotation et son parent(Remplace Instantiate)
        /// </summary>
        /// <param name="gameObject">Le GameObject a activer</param>
        /// <param name="newPos">La nouvelle position</param>
        /// <param name="newRot">La nouvelle rotation</param>
        /// <param name="newParent">Le nouveau parent</param>
        public static void Come(GameObject gameObject, Vector3 newPos, Quaternion newRot, Transform newParent)
        {
            gameObject.transform.parent = newParent;
            gameObject.transform.position = newPos;
            gameObject.transform.rotation = newRot;
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Desactive un GameObject (remplace Destroy)
        /// </summary>
        /// <param name="gameObject">Le GameObject a desactiver</param>
        public static void Leave(GameObject gameObject)
        {
            gameObject.transform.position = Vector3.zero;
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Desactive un GameObject et change son parent (remplace Destroy)
        /// </summary>
        /// <param name="gameObject">Le GameObject a desactiver</param>
        /// <param name="newParent">Le nouveau parent</param>
        public static void Leave(GameObject gameObject, Transform newParent)
        {
            gameObject.transform.position = Vector3.zero;
            gameObject.transform.parent = newParent;
            gameObject.SetActive(false);
        }
    }
}
