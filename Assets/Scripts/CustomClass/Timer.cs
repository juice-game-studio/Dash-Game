using UnityEngine;

namespace CustomClass
{
    /// <summary>
    /// Classe pour créer des countdown et des horloges
    /// </summary>
    public class Timer
    {
        float startTime; // Le temps de départ
        float timeRemaining; // Le temps restant
        bool horlogeStates; // L'état de l'horloge

        /// <summary>
        /// Créer un countdown
        /// </summary>
        /// <param name="startTime">Le temps de départ</param>
        public Timer(float startTime)
        {
            this.startTime = startTime;
            ResetTimer();
        }

        /// <summary>
        /// Créer une horloge
        /// </summary>
        /// <param name="horlogeStates">L'état de base de l'horloge</param>
        public Timer(bool horlogeStates)
        {
            this.horlogeStates = horlogeStates;
        }

        /// <summary>
        /// Lance le countdown
        /// </summary>
        /// <returns>Vrais, si le countdown a finis</returns>
        public bool Countdown()
        {
            timeRemaining -= Time.deltaTime;
            return timeRemaining > 0;
        }

        /// <summary>
        /// Lance l'horloge
        /// </summary>
        public void Horloge()
        {
            if (horlogeStates)
            {
                timeRemaining += Time.deltaTime;
            }
        }

        /// <summary>
        /// Change l'état de l'horloge
        /// </summary>
        /// <param name="states">le nouvelle état de l'horloge</param>
        public void ChangeHorlogeStates(bool states)
        {
            horlogeStates = states;
        }

        /// <summary>
        /// Done le temps restant
        /// </summary>
        /// <returns>Le temps restant</returns>
        public float TimeRemaining()
        {
            return timeRemaining;
        }

        /// <summary>
        /// Done les secondes restantes 
        /// </summary>
        /// <returns>Les secondes restantes</returns>
        public int SecondsRemaining()
        {
            return Mathf.FloorToInt(timeRemaining % 60);
        }

        /// <summary>
        /// Done les minutes restantes
        /// </summary>
        /// <returns>Les minutes restantes</returns>
        public int MinutesRemaining()
        {
            return Mathf.FloorToInt(timeRemaining / 60);
        }

        /// <summary>
        /// Reset le timer
        /// </summary>
        /// <param name="newStartTime">Ajoute ou non un nouveau temps de démarrage</param>
        public void ResetTimer(int newStartTime = 0)
        {
            if (newStartTime == 0)
            {
                timeRemaining = startTime;
            }
            else
            {
                timeRemaining = newStartTime;
            }
        }
    }
}
