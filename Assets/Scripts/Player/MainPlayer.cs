using System.Collections;
using UnityEngine;

/// <summary>
/// Script principale de la gestion du joueur
/// </summary>
public class MainPlayer : MonoBehaviour
{
    #region "Variables"

    [SerializeField] int life = 3; // Les points de vie du joueurs
    int maxLife = 3; // Les points de vie max du joueurs
    [SerializeField] float invulnerabilityTime = 0.5f; // Le temps d'invulnérabilité entre chaque dégat subis

    bool takingDamage = false; // Si le joueur prend est en train de prendre des dégats

    Vector3 defaultPos; // Position du joueur par défaut

    GameManager GM; // Référence du GameManager
    PlayerMouvement PM; // Référence du PlayerMouvement
    SpawnerManager SM; // Référence du SpawnManager

    #endregion

    #region "Variables Acessibility"

    /// <summary>
    /// Retourne le nombre de vie restante jusqu'a la mort du joueur
    /// </summary>
    public int Life
    {
        get { return life; }
        set
        {
            life = value;
            if (life <= 0)
            {
                GM.GameOver();
            }
        }
    }

    /// <summary>
    /// Le nombre max de points de vie
    /// </summary>
    public int MaxLife
    {
        get { return maxLife; }
        set { maxLife = value; }
    }

    #endregion

    private void Start()
    {
        Init();
    }

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    void Init()
    {
        GM = GameManager.instance;
        SM = SpawnerManager.instance;
        defaultPos = transform.position;
        PM = GetComponent<PlayerMouvement>();
        PlayerStates(false);
    }

    /// <summary>
    /// Positionne le player a sa position par défaut
    /// </summary>
    public void DefaultPos()
    {
        transform.position = defaultPos;
    }

    /// <summary>
    /// Change l'état d'activation du player
    /// </summary>
    /// <param name="states">Activer ou désactiver</param>
    public void PlayerStates(bool states)
    {
        gameObject.SetActive(states);
    }

    /// <summary>
    /// Remet les points de vie par défaut
    /// </summary>
    public void ResetLife()
    {
        Life = MaxLife;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GM.InGame == true)
        {
            if (PM.CanDashing == false && takingDamage == false)
            {
                if (collision.gameObject.CompareTag("Enemy"))
                {
                    takingDamage = true;
                    Enemy enemy = collision.GetComponent<Enemy>();
                    int damage = enemy != null ? enemy.DamageToPlayer : 0;
                    TakeDamage(damage);
                }
            }

            if (collision.gameObject.CompareTag("Collectable"))
            {
                collision.GetComponent<Collectable>()?.ApplyEffect();
            }
        }
    }

    /// <summary>
    /// Déclanche la prise de dégat
    /// </summary>
    /// <param name="damage">Le nombre de dégat a subir</param>
    public void TakeDamage(int damage)
    {
        Life -= damage;
        GM.UpdateLifeDisplay(Life);
        StartCoroutine("Invulnerability");
    }

    /// <summary>
    /// Déclanche l'invulnérabilité
    /// </summary>
    IEnumerator Invulnerability()
    {
        GetComponent<SpriteRenderer>().color = Color.yellow;
        yield return new WaitForSeconds(invulnerabilityTime);
        GetComponent<SpriteRenderer>().color = Color.white;
        takingDamage = false;
    }
}
