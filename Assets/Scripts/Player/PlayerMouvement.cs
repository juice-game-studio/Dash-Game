using System.Collections;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.InputSystem;
using Enum.VisualEffect;

/// <summary>
/// Script gérant les déplacement du joueur
/// </summary>
public class PlayerMouvement : MonoBehaviour
{
    #region "Variables"

    [Header("Controle Key")]
    [SerializeField] InputActionReference dashInput; // La référence de l'action jouer pour lancer un dash

    [Header("Mouvement"), Space]
    [SerializeField] float speed = 5; // La vitesse de déplacement du joueur
    [SerializeField] float dashIntensity = 15; // L'intensité du dash du joueur
    [SerializeField] float rotationOffset; // L'offset de rotations appliqué au joueur
    [SerializeField] int dashDamage = 1; // Les degats affligé par le dash
    [Label("Time Between Dash")]
    [SerializeField] public float timeBtwDash = 2; // Le temps entre chaque dash
    [SerializeField, Required] public GameObject dashEffect; // L'effet visuel du dash

    [Header("Clamp"), Space]
    [Label("Min / Max X"), MinMaxSlider(-10f, 10f)]
    [SerializeField] Vector2 rangeX; // La range de clampage en x
    [Label("Min / Max Y"), MinMaxSlider(-10f, 10f)]
    [SerializeField] Vector2 rangeY;  // La range de clampage en y

    bool canDashing = false; // Si le joueur peut dasher ou non
    bool areDashing = false; // Si le joueur est en train de dasher ou non

    float currentSpeed; // Le vitesse actuelle du joueurs

    GameManager GM; // La référence du GameManager

    #endregion

    #region "Variables Acessibility"

    /// <summary>
    /// Retourne l'état du dash
    /// </summary>
    public bool CanDashing
    {
        get { return canDashing; }
    }

    #endregion

    private void Start()
    {
        Init();
    }

    void OnEnable()
    {
        dashInput.action.performed += DoDash;
    }

    void OnDisable()
    {
        dashInput.action.performed -= DoDash;
    }

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    void Init()
    {
        GM = GameManager.instance;
        currentSpeed = speed;
    }

    void Update()
    {
        Movement();
        EditorChanges();
    }

    /// <summary>
    /// Applique les changements de valeurs sur certain variables de l'éditeur
    /// </summary>
    void EditorChanges()
    {
#if UNITY_EDITOR_WIN
        if (currentSpeed != speed && areDashing == false)
        {
            currentSpeed = speed;
        }
#endif
    }

    #region "Global Function"

    /// <summary>
    /// Fonction pour gérer les déplacement du joueur
    /// </summary>
    void Movement()
    {
        // Peut seulement faire des action quand le jeux est lancé ou si le joueur n'est pas mort
        if (GM.InGame == true)
        {
            // Prend la position de la souris 
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 0;
            Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
            mousePos.x = mousePos.x - objectPos.x;
            mousePos.y = mousePos.y - objectPos.y;

            // Change la rotations du joueur pour regarder vers la souris
            float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle + rotationOffset));

            // Change la position du joueur pour aller vers la souris
            Vector3 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPos.z = 0;
            Vector3 transPos = Vector3.MoveTowards(transform.position, targetPos, currentSpeed * Time.deltaTime);

            // Clamp la position du joueur pour ne pas dépassé les bordure du jeux
            transPos.x = Mathf.Clamp(transPos.x, rangeX.x, rangeX.y);
            transPos.y = Mathf.Clamp(transPos.y, rangeY.x, rangeY.y);
            transform.position = transPos;
        }
    }

    /// <summary>
    /// Lance un dash, si le joueur n'est pas déja en train d'en faire un
    /// </summary>
    void DoDash(InputAction.CallbackContext obj)
    {
        if (canDashing == false && GM.InGame)
        {
            canDashing = true;
            StartCoroutine("Dash");
        }
    }

    /// <summary>
    /// Detect qui est sur la trajectoire du dash
    /// </summary>
    /// <param name="posStart">La position de départ</param>
    /// <param name="posEnd">La position de fin</param>
    void DashHit(Vector3 posStart, Vector3 posEnd)
    {
        RaycastHit2D[] dashHit = Physics2D.LinecastAll(posStart, posEnd);
        for (int i = 0; i < dashHit.Length; i++)
        {
            if (dashHit[i].collider != null)
            {
                // Si le joueur touche un ennemie avec son dash, lui applique des dégats
                if (dashHit[i].collider.CompareTag("Enemy") == true)
                {
                    dashHit[i].collider.GetComponent<Enemy>()?.TakeDamage(dashDamage);
                }
            }
        }
    }

    /// <summary>
    /// Take the last position before the dash and the new position after the dash for create a raycast
    /// </summary>
    IEnumerator Dash()
    {
        GetComponent<SpriteRenderer>().color = Color.blue;
        areDashing = true;
        Vector3 currentPos = transform.position;
        currentSpeed *= dashIntensity;

        if (dashEffect != null)
        {
            //Instantiate(dashEffect, transform.position, Quaternion.identity, transform); // A changer ----------------------
            SpawnerManager.instance.SpawnVisualEffect(VisualEffectType.PLAYER_DASH, transform.position, Quaternion.identity, transform);
        }
        else
        {
            Debug.LogWarning(typeof(PlayerMouvement).Name + ": Aucun référence de dashEffect");
        }

        yield return new WaitForSeconds(0.05f);
        currentSpeed = speed;
        DashHit(currentPos, transform.position);

        yield return new WaitForSeconds(0.2f);
        areDashing = false;

        yield return new WaitForSeconds(timeBtwDash - 0.2f);
        canDashing = false;
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    #endregion
}
