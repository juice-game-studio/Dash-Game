using UnityEngine;
using Enum.VisualEffect;

[RequireComponent(typeof(ParticleSystem))]
public class ParticuleManage : MonoBehaviour
{
    [SerializeField] VisualEffectType visualEffectType;

    ParticleSystem PS;

    void Awake()
    {
        PS = GetComponent<ParticleSystem>();
    }

    void OnEnable()
    {
        PS.Play(true);
    }

    void OnParticleSystemStopped()
    {
        SpawnerManager.instance.DestroyVisualEffects(gameObject, visualEffectType);
    }
}
