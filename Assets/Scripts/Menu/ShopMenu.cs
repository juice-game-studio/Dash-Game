using UnityEngine;
using UnityEngine.UI;
using Enum.Menu;
using TMPro;

/// <summary>
/// Script gérant le menu du shop
/// </summary>
public class ShopMenu : MonoBehaviour
{
    #region "Variables"

    [Tooltip("Référence du menu principal")][SerializeField] GameObject menu; // Référence du menu principal
    [Tooltip("Text qui affiche l'argent")][SerializeField] TextMeshProUGUI money; // Référence de l'affichage de l'argent
    [Tooltip("Bouton pour revenir au menu prédécent")][SerializeField] Button back; // Référence du bouton pour revenir au menu prédécent

    UIManager UIM; // Référence de UIManager
    GameManager GM; // Référence de GameManager

    #endregion

    #region "Variables Accessibility"

    /// <summary>
    /// Change l'argent affiché
    /// </summary>
    public int Money
    {
        set { money.text = "Money : " + value; }
    }

    #endregion

    void OnEnable() 
    {
        Money = GM.GlobalMoney;
    }

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    public void Init()
    {
        UIM = UIManager.instance;
        GM = GameManager.instance;
        BindMenu();
    }

    /// <summary>
    /// Associe les boutons au event approprié 
    /// </summary>
    void BindMenu()
    {
        back.onClick.AddListener(delegate { UIM.ActiveMenu(MenuType.MAIN); });
    }

    /// <summary>
    /// Active le menu de jeux
    /// </summary>
    /// <param name="state"></param>
    public void ActiveMenu(bool state)
    {
        menu.SetActive(state);
    }
}
