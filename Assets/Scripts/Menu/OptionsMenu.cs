using UnityEngine;
using UnityEngine.UI;
using Enum.Menu;

/// <summary>
/// Script gérant le menu des options
/// </summary>
public class OptionsMenu : MonoBehaviour
{
    #region "Variables"

    [Tooltip("Référence du menu principal")][SerializeField] GameObject menu; // Référence du menu principal
    [Tooltip("Bouton pour revenir au menu prédécent")][SerializeField] Button back; // Référence du bouton pour revenir au menu prédécent

    UIManager UIM; // Référence de UIManager

    #endregion

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    public void Init()
    {
        UIM = UIManager.instance;
        BindMenu();
    }

    /// <summary>
    /// Associe les boutons au event approprié 
    /// </summary>
    void BindMenu()
    {
        back.onClick.AddListener(delegate { UIM.ActiveMenu(MenuType.MAIN); });
    }

    /// <summary>
    /// Active le menu de jeux
    /// </summary>
    /// <param name="state"></param>
    public void ActiveMenu(bool state)
    {
        menu.SetActive(state);
    }
}
