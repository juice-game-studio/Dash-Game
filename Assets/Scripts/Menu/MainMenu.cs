using UnityEngine;
using UnityEngine.UI;
using Enum.Menu;

/// <summary>
/// Script gérant le menu principal
/// </summary>
public class MainMenu : MonoBehaviour
{
    #region "Variables"

    [Tooltip("Référence du menu principal")][SerializeField] GameObject menu; // Référence du menu principal
    [Tooltip("Bouton pour lancer une partie")][SerializeField] Button play; // Référence du bouton lancer une partie
    [Tooltip("Bouton pour aller dans le shop")][SerializeField] Button shop; // Référence du bouton pour aller dans le shop
    [Tooltip("Bouton pour aller dans les options")][SerializeField] Button options; // Référence du bouton pour aller dans les options
    [Tooltip("Bouton pour quitter le jeux")][SerializeField] Button quit; // Référence du bouton pour quitter le jeux

    UIManager UIM; // Référence de UIManager
    GameManager GM; // Référence de GameManager

    #endregion

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    public void Init()
    {
        GM = GameManager.instance;
        UIM = UIManager.instance;
        BindMenu();
    }

    /// <summary>
    /// Associe les boutons au event approprié 
    /// </summary>
    void BindMenu()
    {
        play.onClick.AddListener(delegate { UIM.ActiveMenu(MenuType.GAME); GM.StartGame(); });
        shop.onClick.AddListener(delegate { UIM.ActiveMenu(MenuType.SHOP); });
        options.onClick.AddListener(delegate { UIM.ActiveMenu(MenuType.OPTIONS); });
        quit.onClick.AddListener(delegate { UIM.QuitGame(); });
    }

    /// <summary>
    /// Active le menu de jeux
    /// </summary>
    /// <param name="state"></param>
    public void ActiveMenu(bool state)
    {
        menu.SetActive(state);
    }
}
