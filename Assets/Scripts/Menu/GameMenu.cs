using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Enum.Menu;
using System;
using TMPro;
using UnityEngine.InputSystem;
using Unity.VisualScripting;

/// <summary>
/// Script gérant le menu de jeux
/// </summary>
public class GameMenu : MonoBehaviour
{
    #region "Variables"

    [SerializeField] GameObject menu; // Référence du menu principal
    [SerializeField] PauseParam pauseParam; // Référence de la class PauseParam
    [SerializeField] GameParam gameParam; // Référence de la class GameParam
    [SerializeField] GameOverParam gameOverParam; // Référence de la class GameOverParam

    /// <summary>
    /// Class stockant les valeurs du menu de pause
    /// </summary>
    [Serializable]
    class PauseParam
    {
        public GameObject pause; // Référence du menu pause
        public InputActionReference pauseInput; // La référence de l'action jouer pour lancer la pause 
        [Tooltip("Bouton pour retourner en jeux")] public Button backToGame; // Référence du bouton pour retourner en jeux
        [Tooltip("Bouton pour aller dans le menu principal")] public Button mainMenu; // Référence du bouton pour aller dans le menu principal
        [Tooltip("Bouton pour quitter le jeux")] public Button quit; // Référence du bouton pour quitter le jeux
    }

    /// <summary>
    /// Class stockant les valeurs du menu de game over
    /// </summary>
    [Serializable]
    class GameOverParam
    {
        public GameObject gameOver; // Référence du menu pause
        [Tooltip("Bouton pour recommencer une partie")] public Button retry; // Référence du bouton pour recommencer une partie
        [Tooltip("Bouton pour aller dans le menu shop")] public Button shop; // Référence du bouton pour aller dans le menu shop
        [Tooltip("Bouton pour quitter le jeux")] public Button quit; // Référence du bouton pour quitter le jeux
    }

    /// <summary>
    /// Class stockant les valeurs du menu de jeux
    /// </summary>
    [Serializable]
    public class GameParam
    {
        public GameObject game; // Référence du menu game
        public TextMeshProUGUI life; // Référence du text affichant les point de vie
        public TextMeshProUGUI money; // Référence du text affichant l'argent
        public TextMeshProUGUI timer; // Référence du text affichant le temps
        public TextMeshProUGUI score; // Référence du text affichant le score
        public TextMeshProUGUI multiplicateur; // Référence du text affichant le multiplicateur de score
        public Slider multiplicateurTime; // Référence du slider affichant le temps restant du multiplicateur de score
        public TextMeshProUGUI difficulty; // Référence du text affichant la difficulté

        /// <summary>
        /// Change les point de vie affiché
        /// </summary>
        public string Life
        {
            set { life.text = "Life : " + value; }
        }

        /// <summary>
        /// Change l'argent affiché
        /// </summary>
        public string Money
        {
            set { money.text = "Money : " + value; }
        }

        /// <summary>
        /// Change le temps de jeux affiché
        /// </summary>
        public string Timer
        {
            set { timer.text = "Time in game : " + value; }
        }

        /// <summary>
        /// Change le score affiché
        /// </summary>
        public string Score
        {
            set { score.text = "Score : " + value; }
        }

        /// <summary>
        /// Change le multiplicateur de score affiché
        /// </summary>
        public string Multiplicateur
        {
            set { multiplicateur.text = "Multiplicateur : " + value; }
        }

        /// <summary>
        /// Change le temps du multiplicateur de score affiché
        /// </summary>
        public float MultiplicateurTimeValue
        {
            set { multiplicateurTime.value = value; }
        }

        /// <summary>
        /// Change le temps max du multiplicateur de score affiché
        /// </summary>
        public float MultiplicateurTimeMaxValue
        {
            set { multiplicateurTime.maxValue = value; }
            get { return multiplicateurTime.maxValue; }
        }

        /// <summary>
        /// Change la difficulté affiché
        /// </summary>
        public string Difficulty
        {
            set { difficulty.text = "Difficulty : " + value; }
        }
    }

    GameManager GM; // Référence de GameManager
    UIManager UIM; // Référence de UIManager

    #endregion

    #region "Variables Accessibility"

    /// <summary>
    /// Retourne les paramètres de jeux
    /// </summary>
    public GameParam Param
    {
        get { return gameParam; }
    }

    #endregion

    void OnEnable()
    {
        pauseParam.pauseInput.action.performed += SetPauseMenu;
    }

    void OnDisable()
    {
        pauseParam.pauseInput.action.performed -= SetPauseMenu;
    }

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    public void Init()
    {
        GM = GameManager.instance;
        UIM = UIManager.instance;
        BindMenu();
        pauseParam.pause.SetActive(false);
    }

    /// <summary>
    /// Associe les boutons au event approprié 
    /// </summary>
    void BindMenu()
    {
        pauseParam.backToGame.onClick.AddListener(delegate { SetPauseMenu(default); });
        pauseParam.mainMenu.onClick.AddListener(delegate { GM.GetPlayer().PlayerStates(false); SetPauseMenu(default); UIM.ActiveMenu(MenuType.MAIN); });
        pauseParam.quit.onClick.AddListener(delegate { UIM.QuitGame(); });

        gameOverParam.retry.onClick.AddListener(delegate { GM.StartGame(); });
        gameOverParam.shop.onClick.AddListener(delegate { GM.GetPlayer().PlayerStates(false); UIM.ActiveMenu(MenuType.SHOP); });
        gameOverParam.quit.onClick.AddListener(delegate { UIM.QuitGame(); });
    }

    /// <summary>
    /// Remet par défaut les UI
    /// </summary>
    public void ResetUI()
    {
        gameParam.Life = GM.GetPlayer().MaxLife.ToString();
        gameParam.Money = "0";
        gameParam.Timer = "0:00";
        gameParam.Score = "0";
        gameParam.Multiplicateur = "1";
        gameParam.MultiplicateurTimeValue = 0;
    }

    /// <summary>
    /// Active le menu de jeux
    /// </summary>
    /// <param name="state"></param>
    public void ActiveMenu(bool state)
    {
        menu.SetActive(state);
    }

    /// <summary>
    /// Change l'état du menu pause
    /// </summary>
    void SetPauseMenu(InputAction.CallbackContext obj)
    {
        if (pauseParam.pause.activeSelf)
        {
            GM.UnPauseGame();
            pauseParam.pause.SetActive(false);
        }
        else
        {
            GM.PauseGame();
            pauseParam.pause.SetActive(true);
        }
    }

    /// <summary>
    /// Active ou désactive le menu de gameOver
    /// </summary>
    /// <param name="states">Le nouvelle état de gameOver</param>
    public void SetGameOverStates(bool states)
    {
        gameOverParam.gameOver.SetActive(states);
    }
}
