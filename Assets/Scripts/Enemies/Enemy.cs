using UnityEngine;
using NaughtyAttributes;
using Enum.Collectable;
using Enum.Enemies;

/// <summary>
/// Classe gérant les capacité basics d'un ennemie
/// </summary>
public class Enemy : MonoBehaviour
{
    #region "Variables"

    [Header("Enemie Basics")]
    [Space]
    [SerializeField] int life = 1; // Les points de vie de l'ennemie
    [SerializeField] int damageToPlayer = 1; // Les degats affligé au joueur
    [SerializeField] float scoreGain = 1; // Le score rajouté a la mort de l'ennemie
    [SerializeField, Range(0, 100)] int chanceToDrop; // La chance de drop un collectable

    protected GameManager GM; // Référence de GameManager
    protected SpawnerManager SM; // Référence de SpawnerManger

    #endregion

    #region "Variables Acessibility"

    /// <summary>
    /// Retourne les degats affligé au player
    /// </summary>
    public int DamageToPlayer
    {
        get { return damageToPlayer; }
    }

    #endregion

    protected virtual void Start()
    {
        GM = GameManager.instance;
        SM = SpawnerManager.instance;
    }

    /// <summary>
    /// Clear le terrain de l'ennemie
    /// </summary>
    public void Clear()
    {
        SpawnerManager.instance.DestroyEnemies(gameObject, EnemiesTypes.HOMING);
    }

    /// <summary>
    /// Déclanche la prise de dégat de l'ennemie
    /// </summary>
    /// <param name="damage">Les dégats a subir</param>
    public void TakeDamage(int damage)
    {
        life -= damage;
        if (life <= 0)
        {
            Death();
        }
    }

    /// <summary>
    /// Déclanche la mort de l'ennemie
    /// </summary>
    public void Death()
    {
        float rand;
        rand = Random.value;

        if (rand * 100 <= chanceToDrop)
        {
            Debug.Log("Spawn");
            SM.SpawnCollectable(transform.position, CollectablesType.MONEY);
        }

        GM.AddScore(scoreGain);
        SM.DestroyEnemies(gameObject, EnemiesTypes.HOMING);
    }
}
