using UnityEngine;
using NaughtyAttributes;

/// <summary>
/// Script gérant un ennemie de type homing
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class HomingEnemy : Enemy
{
    #region "Variables"

    [Header("Mouvement")]
    [Space]
    [SerializeField] float speed = 5f; // La vitesse de déplacement de l'ennemie
    [SerializeField] float rotationSpeed = 200f; // La vitesse de rotations de l'ennemie

    [Header("Clamp")]
    [Space]
    [Label("Min / Max X"), MinMaxSlider(-10f, 10f)]
    [SerializeField] Vector2 rangeX; //  La range de clampage en x
    [Label("Min / Max Y"), MinMaxSlider(-10f, 10f)]
    [SerializeField] Vector2 rangeY; //  La range de clampage en y

    Rigidbody2D RB; // Référence du rigidbody2D
    Transform target; // Référence de la cible a focus

    #endregion

    protected override void Start()
    {
        base.Start();
        RB = GetComponent<Rigidbody2D>();
        target = GM.GetPlayer().transform;
        transform.up = new Vector2(target.position.x - transform.position.x, target.position.y - transform.position.y);
    }

    private void Update()
    {
        //Clamp the position to not quit the screen
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, rangeX.x, rangeX.y), Mathf.Clamp(transform.position.y, rangeY.x, rangeY.y), 0);
    }

    void FixedUpdate()
    {
        if (GM.InGame == true)
        {
            //Take a target and follow him with a rotation speed and velocity
            Vector2 direction = (Vector2)target.position - RB.position;
            direction.Normalize();
            float rotateAmount = Vector3.Cross(direction, transform.up).z;
            RB.angularVelocity = -rotateAmount * rotationSpeed;
            RB.velocity = transform.up * speed;
        }
        else
        {
            RB.velocity = Vector3.zero;
        }
    }

}
