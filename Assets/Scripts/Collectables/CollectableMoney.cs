using UnityEngine;

/// <summary>
/// Script gérant les collectable de type argent
/// </summary>
public class CollectableMoney : Collectable
{
    [Header("Money Parameters")]
    [SerializeField] int moneyValue = 1; // Valeurs de l'argent

    void Awake()
    {
        Init();
    }

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    void Init()
    {
        collectablesType = Enum.Collectable.CollectablesType.MONEY;
    }

    public void AddMoney()
    {
        GM.AddMoney(MoneyValue());
    }

    /// <summary>
    /// Donne la valeurs de l'argent en fonction de la difficulté actuelle
    /// </summary>
    /// <returns></returns>
    int MoneyValue()
    {
        return moneyValue * ((int)GM.GetDifficulty() + 1);
    }
}
