using UnityEngine;
using NaughtyAttributes;
using System.Collections;
using Enum.Collectable;

/// <summary>
/// Script gérant les objets collectables
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class Collectable : MonoBehaviour
{
    [Header("Mouvement")]
    [Space]
    [SerializeField] float speed = 1f; // La vitesse de déplacement
    [Label("Rotation Speed")]
    [SerializeField] float rotationSpeed = 2000f; // La vitesse de rotation
    [Label("Distance To Follow")]
    [SerializeField] float distanceToFollow = 1f; // La distance pour que le collectable suive la cible
    [Label("Time Before Destroy")]
    [SerializeField] float timeBeforeDestroy = 5f; // Le temps avant que le collectable ne soit détruit

    protected CollectablesType collectablesType; // Défini le type de collectables

    Rigidbody2D RB; // Référence du Rigidbody2D
    Transform target; // Référence de la cible a suivre
    protected GameManager GM; // Référence du GameManager
    SpawnerManager SM; // Référence du SpawnerManager

    private void Start()
    {
        Init();
    }

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    void Init()
    {
        GM = GameManager.instance;
        RB = GetComponent<Rigidbody2D>();
        SM = SpawnerManager.instance;
        target = GM.GetPlayer().transform;
    }

    void FixedUpdate()
    {
        // Si la distance pour suivre la cible est suffisament proche, se déplace vers la cible 
        if (Vector3.Distance(target.position, transform.position) < distanceToFollow)
        {
            Vector2 direction = (Vector2)target.position - RB.position;
            direction.Normalize();
            float rotateAmount = Vector3.Cross(direction, transform.up).z;
            RB.angularVelocity = -rotateAmount * rotationSpeed;
            RB.velocity = transform.up * speed;
        }
        else
        {
            RB.velocity = new Vector2(0, 0);
            transform.up = new Vector2(target.position.x - transform.position.x, target.position.y - transform.position.y);
        }
    }

    void OnEnable()
    {
        StartCoroutine(DestroyDelay());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// Détruit l'objet avec un délaie
    /// </summary>
    IEnumerator DestroyDelay()
    {
        yield return new WaitForSeconds(timeBeforeDestroy);
        Destroy();
    }

    void Destroy()
    {
        SM.DestroyCollectables(gameObject, CollectablesType.MONEY);
    }

    /// <summary>
    /// Applique l'effet de l'objet en fonction du type de celui ci
    /// </summary>
    public void ApplyEffect()
    {
        switch (collectablesType)
        {
            case CollectablesType.MONEY:
                GetComponent<CollectableMoney>()?.AddMoney();
                Destroy();
                break;

            default:
                Debug.LogWarning(typeof(Collectable).Name + " : Aucun type de collectables défini");
                Destroy();
                break;
        }
    }
}
