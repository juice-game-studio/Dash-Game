using UnityEngine;
using NaughtyAttributes;
using System;

/// <summary>
/// Script gérant la gestion de la sauvegarde et du chargement des données de jeux
/// </summary>
public class SaveManager : MonoBehaviour
{
    #region "Variables"

    [SerializeField] Values values;

    [Serializable]
    class Values
    {
        public int GlobalMoney;
        public int BestScore;
    }

    GameManager GM; // Référence du GameManager
    public static SaveManager instance; // Référence static du SaveManager

    #endregion

    #region "Manual Fonction"

    /// <summary>
    /// Fonction pour activer manuellement la sauvegarde
    /// </summary>
    [Button] void ManualSave() { SaveValue(); Debug.LogWarning("Sauvegarde éffectué"); }

    /// <summary>
    /// Fonction pour activer manuellement le chargement
    /// </summary>
    [Button] void ManualLoad() { LoadValue(); Debug.LogWarning("Chargement éffectué"); }

    /// <summary>
    /// Fonction pour activer manuellement le reset
    /// </summary>
    [Button] void ManualReset() { ResetSaveValue(); Debug.LogWarning("Reset éffectué"); }

    #endregion

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GM = GameManager.instance;
        LoadValue();
    }

    void OnApplicationQuit()
    {
        SaveValue();
    }

    #region "Global Function"

    public void SaveValue()
    {
        PlayerPrefs.SetInt("GlobalMoney", GM.GlobalMoney);
        PlayerPrefs.SetInt("BestScore", GM.BestScore);
    }

    void LoadValue()
    {
        GM.GlobalMoney = PlayerPrefs.GetInt("GlobalMoney");
        values.GlobalMoney = PlayerPrefs.GetInt("GlobalMoney");

        GM.BestScore = PlayerPrefs.GetInt("BestScore");
        values.BestScore = PlayerPrefs.GetInt("BestScore");
    }

    public void ResetSaveValue()
    {
        GM.GlobalMoney = 0;
        values.GlobalMoney = 0;

        GM.BestScore = 0;
        values.BestScore = 0;

        SaveValue();
    }

    #endregion
}
