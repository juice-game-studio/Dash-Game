using UnityEngine;
using NaughtyAttributes;
using Enum.Difficulty;
using CustomClass;

/// <summary>
/// Script gérant le gameplay
/// </summary>
public class GameManager : MonoBehaviour
{
    #region "Variables"

    [Header("Game"), Space]
    [Label("Game Difficulty")]
    [SerializeField] Difficulty difficulty = Difficulty.EasierThanEasy; // Difficulté en cours
    [SerializeField] bool inGame; // Si le jeux est en cours ou non
    [SerializeField] int gameMoney; // L'argent aquis en jeux
    [SerializeField] int globalMoney; // L'argent totale que le joueur possède

    [Header("Score"), Space]
    [SerializeField] int currentScore; // Le score actuelle
    [SerializeField] int bestScore; // Le meilleur score qu'il a efféctuer
    [SerializeField] int scoreMultiplier; // Le multiplicateur de sont score

    [Header("Objects"), Space]
    [SerializeField, Required] MainPlayer player; // Référence du joueur

    Timer horloge; // Référence de la class Timer de type horloge
    Timer countdown; // Référence de la class Timer de type countdown

    public static GameManager instance; // Référence static de GameManager
    UIManager UIM; // Référence de UIManager
    SpawnerManager SM; // Référence de SpawnManager

    #endregion

    #region "Variables Accessibility"

    /// <summary>
    /// Retourne l'état de la partie en cours
    /// </summary>
    public bool InGame
    {
        get { return inGame; }
    }

    /// <summary>
    /// Retourne et change le meilleur score 
    /// </summary>
    public int BestScore
    {
        get { return bestScore; }
        set { bestScore = value; }
    }

    /// <summary>
    /// Retourne et change l'argent totale du joueur
    /// </summary>
    public int GlobalMoney
    {
        get { return globalMoney; }
        set { globalMoney = value; }
    }

    #endregion

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Init();
    }

    void Update()
    {
        if (inGame)
        {
            UpdateHorloge();
            UpdateCountdown();
        }
    }

    /// <summary>
    /// Initialise les valeurs par défaut
    /// </summary>
    void Init()
    {
        SM = SpawnerManager.instance;
        UIM = UIManager.instance;
        inGame = false;
        horloge = new Timer(false);
        countdown = new Timer(0);
    }

    /// <summary>
    /// Donne le joueur
    /// </summary>
    /// <returns>Le joueur</returns>
    public MainPlayer GetPlayer()
    {
        return player;
    }

    #region "Game Event"

    /// <summary>
    /// Lance le début de la partie
    /// </summary>
    public void StartGame()
    {
        //GetComponent<SpawnerManager>().test();
        SM.ClearAllEntities();
        StartHorloge();
        SetDifficultyDisplay();
        player.PlayerStates(true);
        player.ResetLife();
        player.DefaultPos();
        ResetGameValue();
        UIM.game.SetGameOverStates(false);
        ResetDisplayUI();
        inGame = true;
    }

    /// <summary>
    /// Met la partie en pause
    /// </summary>
    public void PauseGame()
    {
        inGame = false;
    }

    /// <summary>
    /// Enleve la pause de la partie
    /// </summary>
    public void UnPauseGame()
    {
        inGame = true;
    }

    /// <summary>
    /// Déclenche la fin de la partie en cours 
    /// </summary>
    public void GameOver()
    {
        globalMoney += gameMoney;
        UIM.game.SetGameOverStates(true);
        SM.ClearAllEntities();
        player.PlayerStates(false);
        inGame = false;

        if (currentScore > bestScore)
        {
            bestScore = currentScore;
        }

        SaveManager.instance.SaveValue();
    }

    #endregion

    #region "Global Function"

    /// <summary>
    /// Remet a zéro les velurs initiale de jeux
    /// </summary>
    public void ResetGameValue()
    {
        gameMoney = 0;
        currentScore = 0;
        scoreMultiplier = 1;
    }

    /// <summary>
    /// Ajoute des l'argent en jeux
    /// </summary>
    public void AddMoney(int value)
    {
        gameMoney += value;
        UIM.game.Param.Money = gameMoney.ToString();
    }

    /// <summary>
    /// Passe a la difficulté suivante
    /// </summary>
    public void ChangeDifficulty()
    {
        difficulty = (Difficulty)((int)difficulty + 1);
        SetDifficultyDisplay();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public Difficulty GetDifficulty()
    {
        return difficulty;
    }

    #endregion

    #region "Game UI"

    /// <summary>
    /// Affiche la difficulté actuel
    /// </summary>
    void SetDifficultyDisplay()
    {
        UIM.game.Param.Difficulty = difficulty.ToString();
    }

    /// <summary>
    /// Ajoute du score
    /// </summary>
    /// <param name="gain">La valeur du score a ajouter</param>
    public void AddScore(float gain)
    {
        currentScore += Mathf.RoundToInt(gain * ((int)difficulty + 1) * scoreMultiplier);
        scoreMultiplier++;
        countdown.ResetTimer((int)UIM.game.Param.MultiplicateurTimeMaxValue);
        UIM.game.Param.Score = currentScore.ToString();
        UIM.game.Param.Multiplicateur = scoreMultiplier.ToString();
    }

    /// <summary>
    /// Ajoute du temps au countdown
    /// </summary>
    void UpdateCountdown()
    {
        if (countdown.Countdown())
        {
            UIM.game.Param.MultiplicateurTimeValue = countdown.TimeRemaining();
        }
        else
        {
            UIM.game.Param.MultiplicateurTimeValue = 0;
        }
    }

    /// <summary>
    /// Ajoute du temps de l'horloge
    /// <summary>
    void UpdateHorloge()
    {
        horloge.Horloge();
        float minute = horloge.MinutesRemaining();
        int seconds = horloge.SecondsRemaining();
        UIM.game.Param.Timer = string.Format("{0:0}:{1:00}", Mathf.Floor(minute), seconds);
    }

    /// <summary>
    /// Démarre l'horloge
    /// </summary>
    void StartHorloge()
    {
        horloge.ResetTimer();
        horloge.ChangeHorlogeStates(true);
    }

    /// <summary>
    /// Change l'affichage des points de vie
    /// </summary>
    /// <param name="life">Nouvelle valeurs des points de vie</param>
    public void UpdateLifeDisplay(int life)
    {
        UIM.game.Param.Life = life.ToString();
    }

    /// <summary>
    /// Remet les UI de jeux par défaut
    /// </summary>
    void ResetDisplayUI()
    {
        UIM.game.ResetUI();
    }

    #endregion
}
