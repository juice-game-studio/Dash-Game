using UnityEngine;
using Enum.Menu;

/// <summary>
/// Script gérant tout les différents menu
/// </summary>
public class UIManager : MonoBehaviour
{
    #region "Variables"

    [Header("Menus"), Space]
    public MainMenu main; // Menu principale
    public OptionsMenu options; // Menu des options
    public GameMenu game; // Menu en jeux
    public ShopMenu shop; // Menu du shop

    public static UIManager instance; // Référence static du UIManager

    #endregion

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        InitAllMenu();
        ActiveMenu(MenuType.MAIN);
    }

    #region "Global Function"

    /// <summary>
    /// Active le menu selectioné
    /// </summary>
    /// <param name="type">Definit le type de menu a activer</param>
    public void ActiveMenu(MenuType type)
    {
        switch (type)
        {
            case MenuType.MAIN:
                ActiveMainMenu();
                break;

            case MenuType.OPTIONS:
                ActiveOptionsMenu();
                break;

            case MenuType.SHOP:
                ActiveShopMenu();
                break;

            case MenuType.GAME:
                ActiveGameMenu();
                break;

            default:
                break;
        }
    }

    /// <summary>
    /// Initialise tout les menus
    /// </summary>
    void InitAllMenu()
    {
        main.Init();
        options.Init();
        game.Init();
        shop.Init();
    }

    /// <summary>
    /// Quitte le jeux (fonctionne aussi in play mode)
    /// </summary>
    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }

    #endregion

    #region "Menu Activation"

    /// <summary>
    /// Active le main menu
    /// </summary>
    void ActiveMainMenu()
    {
        main.ActiveMenu(true);
        options.ActiveMenu(false);
        game.ActiveMenu(false);
        shop.ActiveMenu(false);
    }

    /// <summary>
    /// Active le menu des options
    /// </summary>
    void ActiveOptionsMenu()
    {
        main.ActiveMenu(false);
        options.ActiveMenu(true);
        game.ActiveMenu(false);
        shop.ActiveMenu(false);
    }

    /// <summary>
    /// Active le menu du jeux
    /// </summary>
    void ActiveGameMenu()
    {
        main.ActiveMenu(false);
        options.ActiveMenu(false);
        game.ActiveMenu(true);
        shop.ActiveMenu(false);
    }

    /// <summary>
    /// Active le menu du shop
    /// </summary>
    void ActiveShopMenu()
    {
        main.ActiveMenu(false);
        options.ActiveMenu(false);
        game.ActiveMenu(false);
        shop.ActiveMenu(true);
    }

    #endregion
}

