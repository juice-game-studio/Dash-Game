using UnityEngine;
using NaughtyAttributes;
using System;

/// <summary>
/// Script gérant différants debug et controle manuelle
/// </summary>
public class DebugManager : MonoBehaviour
{
    /// <summary>
    /// Classe contenant les différentes valeurs de debug
    /// </summary>
    [Serializable]
    class Values
    {
        public int damageTaken; // Les dégats a faire prendre
        public int scoreAdded; // Le score a ajouter
        public int moneyAdded; // L'argent a ajouter
        public int globalMoneyAdded; // L'argent globale a ajouter
    }

    [SerializeField] Values values; // Référence de la classe Values


    /// <summary>
    /// Ajoute du score manuellement
    /// </summary>
    [Button] void AddScore() { GM.AddScore(values.scoreAdded); Debug.Log("Ajout de " + values.scoreAdded + " au score"); }

    /// <summary>
    /// Ajoute de l'argent manuellement
    /// </summary>
    [Button] void AddMoney() { GM.AddMoney(values.moneyAdded); Debug.Log("Ajout de " + values.moneyAdded + " a l'argent"); }

    /// <summary>
    /// Ajoute de l'argent globale manuellement
    /// </summary>
    [Button] void AddGlobalMoney() { GM.GlobalMoney += values.globalMoneyAdded; Debug.Log("Ajout de " + values.globalMoneyAdded + " a l'argent globale"); }

    /// <summary>
    /// Ajoute des dégats subis manuellement
    /// </summary>
    [Button] void TakeDamage() { player.TakeDamage(values.damageTaken); Debug.Log("Ajout de " + values.damageTaken + " au dégat subis"); }

    /// <summary>
    /// Change la difficulté
    /// </summary>
    [Button] void ChangeDifficulty() { GM.ChangeDifficulty(); Debug.Log("Changement de difficulté"); }

    /// <summary>
    /// Lance le Game Over
    /// </summary>
    [Button] void GameOver() { GM.GameOver(); Debug.Log("Lancement de Game Over"); }

    MainPlayer player; // Référence de MainPlayer
    GameManager GM; // Référence de GameManager

    void Start()
    {
        GM = GameManager.instance;
        player = GM.GetPlayer();
    }
}
