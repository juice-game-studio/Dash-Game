using System.Collections;
using UnityEngine;
using NaughtyAttributes;
using System;
using Enum.Enemies;
using Enum.Collectable;
using Enum.VisualEffect;
using static CustomClass.ComeAndLeave;

/// <summary>
/// Script gérant le spawn des différants entités
/// </summary>
public class SpawnerManager : MonoBehaviour
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    class Enemies
    {
        public Transform homingEnemiesHolder; //
        public GameObject[] homingEnemies; //
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    class Collectables
    {
        public Transform moneyHolder; //
        public GameObject[] money; //
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    class VisualEffects
    {
        public Transform playerEffectHolder; //
        public GameObject playerDash; //
    }

    [SerializeField] Enemies enemies; //
    [SerializeField] Collectables collectables; //
    [SerializeField] VisualEffects visualEffects; //

    public static SpawnerManager instance; // Référence static de SpawnerManager
    GameManager GM; // 

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GM = GameManager.instance;
    }

    /// <summary>
    /// Spawn un ennemie en dehors de la range du joueur
    /// </summary>
    /// <param name="enemy">L'ennemie a instancier</param>
    /// <param name="type"></param>
    public void SpawnEnemy(EnemiesTypes type)
    {
        GameObject enemy = GetEnemiesType(type);
        if (enemy != null)
        {
            Vector3 randPos = new Vector3(UnityEngine.Random.Range(-8.5f, 8.5f), UnityEngine.Random.Range(-4.5f, 4.5f), 0);
            if (Vector3.Distance(GM.GetPlayer().transform.position, randPos) < 5)
            {
                Come(enemy);
            }
            else
            {
                Come(enemy, randPos, Quaternion.identity);
            }
        }
    }

    /// <summary>
    /// Spawn un collectable sur la position donnée
    /// </summary>
    /// <param name="type"></param>
    public void SpawnCollectable(Vector3 pos, CollectablesType type)
    {
        GameObject collectable = GetCollectablesType(type);
        if (collectable != null)
        {
            Come(collectable, pos, Quaternion.identity);
        }
    }

    /// <summary>
    /// Spawn un collectable sur la position donnée
    /// </summary>
    /// <param name="type"></param>
    public void SpawnVisualEffect(VisualEffectType type, Vector3 pos, Quaternion rot = default(Quaternion), Transform newParent = null)
    {
        GameObject visualEffect = GetVisualEffectType(type);
        if (visualEffect != null)
        {
            if (newParent != null)
            {
                Come(visualEffect, pos, rot, newParent);
            }
            else
            {
                Come(visualEffect, pos, rot);
            }
        }
    }

    public void ClearAllEntities()
    {
        ClearEntities(enemies.homingEnemies);
        ClearEntities(collectables.money);
    }

    void ClearEntities(GameObject[] entity)
    {
        for (int i = 0; i < entity.Length; i++)
        {
            if (entity[i].activeSelf == true)
            {
                Destroy(entity[i]);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="type"></param>
    public void DestroyEnemies(GameObject entity, EnemiesTypes type)
    {
        switch (type)
        {
            case EnemiesTypes.HOMING:
                Leave(entity, enemies.homingEnemiesHolder);
                break;

            default:
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="type"></param>
    public void DestroyCollectables(GameObject entity, CollectablesType type)
    {
        switch (type)
        {
            case CollectablesType.MONEY:
                Leave(entity, collectables.moneyHolder);
                break;

            default:
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="type"></param>
    public void DestroyVisualEffects(GameObject entity, VisualEffectType type)
    {
        switch (type)
        {
            case VisualEffectType.PLAYER_DASH:
                Leave(entity, visualEffects.playerEffectHolder);
                break;

            default:
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    int GetEntityNotActivated(GameObject[] entity)
    {
        for (int i = 0; i < entity.Length; i++)
        {
            if (entity[i].activeSelf == false)
            {
                return i;
            }
        }
        Debug.LogWarning(typeof(SpawnerManager).Name + ": Le nombre d'entity de type '" + entity[0].name + "', est déja présent au maximum");
        return -1;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    GameObject GetEnemiesType(EnemiesTypes type)
    {
        switch (type)
        {
            case EnemiesTypes.HOMING:
                int value = GetEntityNotActivated(enemies.homingEnemies);
                return value != -1 ? enemies.homingEnemies[value] : null;
            default:
                return null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    GameObject GetCollectablesType(CollectablesType type)
    {
        switch (type)
        {
            case CollectablesType.MONEY:
                int value = GetEntityNotActivated(collectables.money);
                return value != -1 ? collectables.money[value] : null;
            default:
                return null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    GameObject GetVisualEffectType(VisualEffectType type)
    {
        switch (type)
        {
            case VisualEffectType.PLAYER_DASH:
                return visualEffects.playerDash;
            default:
                return null;
        }
    }

    [Button]
    public void test()
    {
        StartCoroutine(tests());
    }

    /// <summary>
    /// 
    /// </summary>
    IEnumerator tests()
    {
        if (GM.InGame == true)
        {
            int value = GetEntityNotActivated(enemies.homingEnemies);
            if (value != -1)
            {
                SpawnEnemy(EnemiesTypes.HOMING);
            }
            yield return new WaitForSeconds(1f);
            StartCoroutine(tests());
        }
    }
}
